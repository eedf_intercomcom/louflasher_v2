# -*- encoding: utf-8 -*-
#
#   LouFlasher v2 - outil d'édition de newsletter des éclés
#
#   Copyright © 2014-2023  Florence Birée <florence@biree.name>
#   Copyright © 2014-2015  Émile Decorsière <al@md2t.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

class Command(BaseCommand):
    help = "Set an existing user as admin"

    def add_arguments(self, parser):
        parser.add_argument("username")

    def handle(self, *args, **options):
        try:
            user = User.objects.get(username=options["username"])
        except User.DoesNotExist:
            raise CommandError('User {} does not exist'.format(options["username"]))
        
        user.is_staff = True
        user.is_superuser = True
        user.save()
        
        self.stdout.write(
            self.style.SUCCESS('Successfully updated user {}'.format(options["username"]))
        )

