# -*- encoding: utf-8 -*-
#
#   LouFlasher v2 - outil d'édition de newsletter des éclés
#
#   Copyright © 2014-2023  Florence Birée <florence@biree.name>
#   Copyright © 2014-2015  Émile Decorsière <al@md2t.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.urls import re_path, path
from django.contrib.auth import views as auth_views
from flash import views

urlpatterns = [
    # auth urls
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='registration/logout.html'), name='logout'),
    # index des newsletters
    path('', views.index, name='index'),
    # paramètres d'une newsletter
    path('params/', views.params, name="newflash"),
    path('params/<slug:slug>/', views.params, name="flashparams"),
    path('dest/<slug:slug>/', views.dest, name="flashdest"),
    # édition d'une newsletter
    path('<slug:slug>/', views.flashview, name="flash"),
    path('<slug:slug>/delete/<int:pk>/', views.delete, name="delinfo"),
    path('<slug:slug>/edit/<int:pk>/', views.editinfo, name="editinfo"),
    path('<slug:slug>/testsend/', views.testsend, name="testsend"),
    path('<slug:slug>/send/', views.send, name="send"),
]

