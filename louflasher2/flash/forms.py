# -*- encoding: utf-8 -*-
#
#   LouFlasher v2 - outil d'édition de newsletter des éclés
#
#   Copyright © 2014-2023  Florence Birée <florence@biree.name>
#   Copyright © 2014-2015  Émile Decorsière <al@md2t.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.forms import CharField, ModelForm, TextInput, CheckboxSelectMultiple, Form, EmailField
from flash.models import Flash, Destinataire, LouInfo
from django.core.exceptions import ValidationError

class FlashForm(ModelForm):
    """Formulaire de création ou d'édition des paramètres d'un flash"""
    
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(FlashForm, self).__init__(*args, **kwargs)
    
    auteurices = CharField(
        label="Comptes Galilée ayant accès à cette newsletter",
        help_text="Nom d'utilisateurice séparés par une espace."
    )
    
    class Meta:
        model = Flash
        fields = [
            'nom', 'from_mail', 'to_mail', 'titre',
            'bandeau', 'bandeau_alt', 'entete', 'pied'
        ]
        
    def clean_auteurices(self):
        auteurices = self.cleaned_data['auteurices']
        auteurices_list = auteurices.split()
        if self.request.user.username not in auteurices_list:
            raise ValidationError(
                'Vous ne pouvez pas vous enlever des comptes ayant accès à cette newsletter',
                code='cant_remove_myself',
            )
        # Always return a value to use as the new cleaned data, even if
        # this method didn't change it.
        return auteurices

class LouInfoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.flash = kwargs.pop('flash', None)
        super(LouInfoForm, self).__init__(*args, **kwargs)
        
        self.fields["qui"].queryset = self.flash.destinataire_set.all()
         
        
    class Meta:
        model = LouInfo
        exclude = ['flash']
        widgets = {
            'qui': CheckboxSelectMultiple(),
        }
        
class LouFactourForm(Form):
    testmail = EmailField(label="Adresse e-mail")
