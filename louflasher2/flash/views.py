# -*- encoding: utf-8 -*-
#
#   LouFlasher v2 - outil d'édition de newsletter des éclés
#
#   Copyright © 2014-2023  Florence Birée <florence@biree.name>
#   Copyright © 2014-2015  Émile Decorsière <al@md2t.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy, reverse
from flash.models import Flash, GUser, Destinataire, LouInfo
from flash.forms import FlashForm, LouInfoForm, LouFactourForm
from django.conf import settings
from django.forms import formset_factory, modelformset_factory, TextInput
from django.db.models import Count
from django.http import Http404
from datetime import datetime
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from email.mime.image import MIMEImage

# utility functions
def get_flash_or_404(request, slug=None):
    """Return only flash for current user.
    
        If slug, return the corresponding flash
        else, return all flash of this user
       
    """
    if slug:
        return get_object_or_404(Flash,
            slug=slug,
            auteurices__uid__iexact=request.user.username
        )
    else:
        return Flash.objects.filter(
            auteurices__uid__iexact=request.user.username
        )

def get_infos(flash):
    """Récupérer la liste des LouInfo dans le bon ordre"""
    return LouInfo.objects.filter(flash=flash).annotate(nb=Count('qui')).order_by('-nb')

def titre_formate(flash):
    """Formatage du titre de la newsletter"""
    today = datetime.today()
    return flash.titre.format(
        semaine=today.isocalendar()[1], 
        date=today.strftime("%x"), 
        mois=today.month,
        annee=today.year,
    )

def sendlouflash(flash, to=None):
    """Envoyer une newsletter,
        à l'adresse `to` si spécifiée
        à l'adresse flash.to_mail sinon    
    """
    if not to:
        to = flash.to_mail
    
    # formatage du titre
    subject = titre_formate(flash)
    
    render_vars = {
        'liste_info': get_infos(flash),
        'dest_qui_existent': flash.destinataire_set.all(),
        'flash': flash
    }
    
    html_content = render_to_string('flash/mail.html', render_vars)
    text_content = render_to_string('flash/mail.txt', render_vars)
    
    msg = EmailMultiAlternatives(subject, text_content, flash.from_mail, [to])
    msg.attach_alternative(html_content, "text/html")

    if flash.bandeau:
        msg.mixed_subtype = 'related'

        fp = flash.bandeau.open()
        msgImage = MIMEImage(fp.read())
        flash.bandeau.close()

        msgImage.add_header('Content-ID', '<bandeau>')
        msg.attach(msgImage)

    msg.send()


@login_required(login_url=reverse_lazy('login'))
def index(request):
    """Affiche la page d'accueil de louflasher, qui liste les newsletter"""
    
    # récup liste des newsletters
    flash_list = get_flash_or_404(request)
    return render(request, 'flash/index.html', {
        'flash_list': flash_list,
        'request': request,
    })

@login_required(login_url=reverse_lazy('login'))
def params(request, slug=None):
    """Affiche la page de paramétrage d'un flash, en créé un si slug=None"""
    
    if request.method == 'POST':
        if slug:
            flash = get_flash_or_404(request, slug) 
            form = FlashForm(request.POST, request.FILES, request=request, instance=flash)
        else:
            form = FlashForm(request.POST, request.FILES, request=request)
            
        if form.is_valid():
            # sauver le form
            flash = form.save() # créé le slug si besoin
            # mettre à jour la liste de noms d'utilisateurices
            auteurices_list = form.cleaned_data['auteurices'].split()
            guser_list = []
            for auteurice in auteurices_list:
                # check this user exists
                try:
                    u = GUser.objects.get(uid=auteurice)
                except GUser.DoesNotExist:
                    u = GUser(uid=auteurice)
                    u.save()
                guser_list.append(u)
            flash.auteurices.set(guser_list)
            flash.save()
            
            # si newflash: ajouter données initiales de destinataires
            if slug is None:
                for couleur, nom in settings.CAT_INIT:
                    d = Destinataire(couleur=couleur, nom=nom, flash=flash)
                    d.save()
            
            return redirect(flash)
    else:
        if slug:
            # vérifier qu'on a accès à cette newsletter, sinon 404
            flash = get_flash_or_404(request, slug)
            # get auteurices
            auteurices = ""
            for guser in flash.auteurices.all():
                auteurices = auteurices + guser.uid + " "
            form = FlashForm(instance=flash, initial={'auteurices': auteurices}, request=request)
        else:
            # sinon ajouter au moins le nom d'ulisateurice actuel
            form = FlashForm(initial={'auteurices': request.user.username}, request=request)
    return render(request, 'flash/params.html', {
        'form': form,
        'slug': slug,
        'request': request,
    })

@login_required(login_url=reverse_lazy('login'))                                 
def dest(request, slug):
    """Vue pour éditer les destinataires d'un flash"""
    flash = get_flash_or_404(request, slug)

    DestFormSet = modelformset_factory(
        Destinataire,
        fields=['nom', 'couleur'],
        can_delete=True,
        widgets = {
            'couleur': TextInput(attrs={'data-coloris':''}),
        },
    )
    
    if request.method == 'POST':
        formset = DestFormSet(request.POST, request.FILES, queryset=flash.destinataire_set.all())
        if formset.is_valid():
            # do something with the formset.cleaned_data
            instances = formset.save(commit=False)
            # create or update new or changed dest
            for dest in instances:
                dest.flash = flash
                dest.save()
            # delete dest to be deleted
            for dest in formset.deleted_objects:
                dest.delete()
            return redirect(reverse('flashdest', args=[slug]))
    else:
        formset = DestFormSet(queryset=flash.destinataire_set.all())

    return render(request, 'flash/flashdest.html', {
        'flash': flash,
        'formset': formset,
        'request': request,
    })

@login_required(login_url=reverse_lazy('login'))                                 
def flashview(request, slug):
    """Vue pour composer un flash"""
    flash = get_flash_or_404(request, slug)

    if request.method == 'POST': # If the form has been submitted...
        form = LouInfoForm(request.POST, request.FILES, flash=flash) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            info = form.save(commit=False)
            info.flash = flash
            info.save()
            form.save_m2m()
            
            return redirect(reverse('flash', args=[slug]))
    else:
        form = LouInfoForm(flash=flash) # An unbound form
    
    return render(request, 'flash/flashview.html', {
        'form': form,
        'liste_info': get_infos(flash),
        'dest_qui_existent': flash.destinataire_set.all(),
        'msg': request.GET['msg'] if 'msg' in request.GET else None,
        'factour': LouFactourForm(),
        'flash': flash,
        'subject': titre_formate(flash),
        'request': request,
    })

@login_required(login_url=reverse_lazy('login'))
def delete(request, slug, pk):
    """Suppression d'une info"""
    flash = get_flash_or_404(request, slug)
    
    info = get_object_or_404(LouInfo, pk=pk)
    info.delete()
    return redirect(reverse('flash', args=[slug]) + '?msg=delete')

@login_required(login_url=reverse_lazy('login'))
def testsend(request, slug):
    """Vue d'envoi d'un mail de test"""
    flash = get_flash_or_404(request, slug)
    
    if request.method == 'POST':
        form = LouFactourForm(request.POST)
        if form.is_valid():
            # send to form testmail
            sendlouflash(flash, form.cleaned_data['testmail'])
            return redirect(reverse('flash', args=[slug]) + '?msg=testok')
        else:
            return redirect(reverse('flash', args=[slug]) + '?msg=nottest')
    raise Http404
    
@login_required(login_url=reverse_lazy('login'))
def send(request, slug):
    """Vue d'envoi de la newsletter, puis suppression des infos"""
    flash = get_flash_or_404(request, slug)
    
    # envoi du flash
    sendlouflash(flash)
    # suppression du bordel
    LouInfo.objects.filter(flash=flash).delete()
    # et retour au début
    return redirect(reverse('flash', args=[slug]) + '?msg=sendok')

@login_required(login_url=reverse_lazy('login'))
def editinfo(request, slug, pk):
    """Edition d'une info"""
    flash = get_flash_or_404(request, slug)
    
    info = get_object_or_404(LouInfo, pk=pk)
    
    if request.method == 'POST': # If the form has been submitted...
        form = LouInfoForm(request.POST, request.FILES, flash=flash, instance=info) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            info = form.save(commit=False)
            info.flash = flash
            info.save()
            form.save_m2m()
            
            return redirect(reverse('flash', args=[slug]))
    else:
        form = LouInfoForm(flash=flash, instance=info)
    
    return render(request, 'flash/editinfo.html', {
        'form': form,
        'flash': flash,
        'request': request,
    })
