# -*- encoding: utf-8 -*-
#
#   LouFlasher v2 - outil d'édition de newsletter des éclés
#
#   Copyright © 2014-2023  Florence Birée <florence@biree.name>
#   Copyright © 2014-2015  Émile Decorsière <al@md2t.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from datetime import date
from django.db import models
from django.urls import reverse
from django.template.defaultfilters import slugify
from flash.utils import unique_slugify

class GUser(models.Model):
    """Utilisateurice Galilée"""
    uid = models.CharField("Nom d'utilisateurice Galilée", max_length=256)
    
    def __str__(self):
        return self.uid

class Flash(models.Model):
    """Un flash (une newsletter)"""
    def get_bandeau_path(instance, filename):
        return "louflash2/{}/{}".format(
            instance.slug,
            filename,
        )
    
    # le flash
    nom = models.CharField("Nom de la newsletter", max_length=100)
    slug = models.SlugField(max_length=100)
    auteurices = models.ManyToManyField(GUser, verbose_name="Administrateurices de la liste")
    
    
    # les infos pour le mail
    from_mail = models.EmailField("E-mail d'expéditeurice « De » pour la newsletter")
    to_mail = models.EmailField("E-mail de destination « À » (liste e-mail)")
    titre = models.CharField("Titre de la newsletter", max_length=256,
        help_text="Tu peux utiliser {semaine} qui sera remplacé par le numéro de semaine, {date} qui sera remplacé par la date, {mois} par le jour du mois et {annee} par l'année.")
    entete = models.TextField("Texte d'en-tête de la newsletter", blank=True)
    pied = models.TextField("Texte de pied de page de la newslette", blank=True)
    bandeau = models.ImageField(upload_to=get_bandeau_path, blank=True,
        help_text="Taille conseillée : 800x220px")
    bandeau_alt = models.CharField("Équivalent textuel du bandeau", max_length=256,
        blank=True, help_text="Pour les personnes malvoyantes, utilisant un lecteur d'écran")
    
    def get_absolute_url(self):
        return reverse('flash', args=[self.slug])
        
    def save(self, *args, **kwargs):  # new
        if not self.slug:
            unique_slugify(self, self.nom)
        return super().save(*args, **kwargs)
    
    def __str__(self):
        return '{} ({})'.format(self.nom, self.to_mail)

class Destinataire(models.Model):
    """Catégories de destinataires d'une info"""
    couleur = models.CharField(max_length=7)
    nom = models.CharField(max_length=50)
    flash = models.ForeignKey(Flash, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nom
        #try:
        #    return '{}>{}'.format(self.flash, self.nom)
        #except:
        #    return '[NoFlashYet]>{}'.format(self.nom)
        
    
    def abbr(self):
        return ''.join(lettre for lettre in self.nom if lettre.isupper())
        
    def hexcol(self):
        """Renvoie juste le code hexa de la couleur sans le #"""
        return self.couleur[1:]

class LouInfo(models.Model):
    """Une info à envoyer"""
    def get_fichier_path(instance, filename):
        return "louflash2/{}/{}/{}/{}".format(
            instance.flash.slug,
            date.today().year,
            date.today().isocalendar()[1],
            filename
        )
    
    flash = models.ForeignKey(Flash, on_delete=models.CASCADE)
    qui = models.ManyToManyField("Destinataire", verbose_name="Pour qui")
    titre =  models.CharField(max_length=250)
    description = models.TextField()
    url = models.URLField(max_length=200, blank=True)
    fichier = models.FileField(upload_to=get_fichier_path, blank=True)
    
    def __str__(self):
        return self.titre

