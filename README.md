=============
LouFlasher v2
=============

LouFlasher est une interface web pour construire des mails d'information.

Principe
========

Pour chaque information à ajouter, on se connecte à LouFlasher, et on remplit
un formulaire avec les informations suivantes :
    * un titre
    * à qui ça s'adresse (dans une liste pré-déterminée)
    * une description
    * éventuellement une URL ou un fichier joint (qui sera envoyé sous la forme
      d'un lien vers un fichier à télécharger)

L'envoi se fait manuellement en cliquant sur un bouton, qui assemble le mail et
l'envoie à une adresse pré-définie (une liste mail, par exemple). Lors de 
l'envoi, la liste d'informations en attente est vidée.

Les mails sont envoyés en remplissant des templates, et en deux formats, texte
et HTML.

Avant l'envoi général, on peut s'envoyer un mail de test, à une adresse 
quelconque.

Dépendances
===========

LouFlasher v2 a été conçu pour Django 3.2. Il nécessite un serveur web 
(Apache, Nginx) avec un système pour exécuter des applications web en python.

Dépend aussi de :

  * django-auth-ldap

Mise en œuvre
=============

Le répertoire `louflasher2` contient le projet django, à configurer dans 
`louflasher2/settings.py`.

L'exemple de `settings.py` propose une authentification via LDAP.
